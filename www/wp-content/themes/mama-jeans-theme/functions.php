<?php 
// error_reporting(E_ERROR | E_PARSE);

// HIDE ADMIN BAR ALWAYS
//add_filter( 'show_admin_bar', '__return_false' );

add_action( 'wp_enqueue_scripts', 'no_wp_jquery' );
function no_wp_jquery(){
	wp_deregister_script('jquery');
}

//CLEAN UP WP HEAD
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');


// POST THUMBNAILS (FEATURED IMAGE)
add_theme_support( 'post-thumbnails' );



// CUSTOM MENU REGISTRATION
add_action( 'init', 'register_top_menu' );
function register_top_menu() {
register_nav_menu( 'mama-jeans', __( 'Primary Nav Menu' ) );
 }

add_action( 'init', 'register_bottom_menu' );
function register_bottom_menu() {
register_nav_menu( 'mama-jeans-footer', __( 'Secondary Nav Menu' ) );
 }



// SIDEBARS
// register_sidebar(array(
// 	'name'          => 'Blog Sidebar',
// 	'id'            => 'sidebar-blog',
// 	'description'   => '',
// 	'before_widget' => '<div id="%1$s" class="widget %2$s">',
// 	'after_widget'  => '</div><div class="doubledot_hr"></div>',
// 	'before_title'  => '<h3 class="widgettitle">',
// 	'after_title'   => '</h3>' ));
	


// CUSTOM POST TYPES
include_once('includes/custom_post_types.php');


// CUSTOM TAXONOMIES
include_once('includes/custom_taxonomies.php');


// WIDGETS
//include_once('widgets/init.php');


// THIRD PARTY INTERFACES
//include_once('includes/thirdparty.php');


// UTILITY & MISC FUNCTIONS
include_once('includes/utility_funcs.php');


// SECURITY STUFF
define('DISALLOW_FILE_EDIT', true);

//Post Popularity Counter
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

//Add "Read More" and link to the excerpt
function new_excerpt_more( $more ) {
    return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read More...</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );