<?php
	/*
	 * Template Name: Home Page
	 * Creates the home page for MaMa Jean's
	 */
	get_header();
	the_post();
?>
<section class="inner-section">
	<div class="slider">
		<img src="<?php echo get_template_directory_uri(); ?>/images/slider-img1.png"/>
	</div>

	<div class="tagline">
		<h3>-What Food Should Be-</h3>
	</div>

	<div class="features">
			<ul class="cf">
				<?php if( get_field('featured_image_list') ): while( has_sub_field('featured_image_list') ): ?>

						<li>
							<a href="<?php get_sub_field('link');?>"><img src="<?php the_sub_field('featured_image'); ?>"/></a>
						</li>

					<?php endwhile; endif; ?>
			</ul>
	</div>

	<div class="divider grey">
	</div>
	<div class="flexwrap fwmain cf">
		<div class="events v-left">
		<h3>Upcoming Events</h3>
		<div class="white divider"></div>
		<!-- Get the post fields for two upcoming events: mj_event_date , the_title, the_excerpt -->
		<article>
			<h3></h3>
			<h4>Title</h4>
			<p>Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt. Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt. Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt. <a href="#">Read More</a></p>
		</article>
		<article>
			<h3>Date</h3>
			<h4>Title</h4>
			<p>Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt Excerpt. <a href="#">Read More</a></p>
		</article>
		<h4><a href="<?php echo get_permalink();?>/community/">More Events<img src="<?php echo get_template_directory_uri(); ?>/images/events-arrow.png"/></a></h4>
		</div>
		<div class="events-right v-right">
			<div class="mysterybox">
				<!-- What goes here? No one knows but it disappears on the mobile site -->
			</div>
			<div class="divider grey"></div>
			<div class="newsletter">
				<h3>Know what's up!</h3>
				<p>sign up for our monthly newsletter!</p>
				<!-- Contact Form wizardry -->
			</div>
				<div class="divider grey"></div>
			<div class="follow">
				<h3>Follow us!</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/pinterest.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/googleplus.png" />
					<img src="<?php echo get_template_directory_uri(); ?>/images/rss.png" />
			</div>
			<div class="divider grey"></div>
		</div>
	</div>
</section>
<?php get_footer() ?>