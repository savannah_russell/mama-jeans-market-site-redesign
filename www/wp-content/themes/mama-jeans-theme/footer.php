<footer>
	<div class="oc-border">
	</div>
	<?php wp_nav_menu(array('menu' => 'mamas-footer-menu', 'container' => 'nav', 'container_class' => 'footnav cf', 'after' => '<li class="dot">&#8226;</li>'));?>
	<a href="#return" class="cf"><img src="<?php echo get_template_directory_uri(); ?>/images/to-top.png"/></a>
	<a href="<?php echo esc_url( home_url('/') ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logo.png"/></a>
	<div class="copyright">
		<p>&#169;2013 MaMa Jean's Natural Market</p>
	</div>
</footer>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="<?php bloginfo('template_directory') ?>/js/js.js"></script>
	<script type="text/javascript">		
			$(".headnav").addClass("js").before('<a href="#" id="menu-icon"></a>');
			$("#menu-icon").click(function(){
				$(".headnav").slideToggle();
			});
			$(".sales-dropdown-grocery").click(function(){
				$(".sales-hide-grocery").slideToggle();
			});
			$(".sales-dropdown-supplements").click(function(){
				$(".sales-hide-supplements").slideToggle();
			});
			$(".sales-dropdown-personal").click(function(){
				$(".sales-hide-personal").slideToggle();
			});
			$(".sales-dropdown-beer").click(function(){
				$(".sales-hide-beer").slideToggle();
			});
			$(".sales-dropdown-bulk").click(function(){
				$(".sales-hide-bulk").slideToggle();
			});
			$(window).resize(function(){
				if(window.innerWidth > 768) {
					$(".headnav").removeAttr("style");
				}
			});
	
	</script>
	<?php wp_footer() ?>
</body>
</html>