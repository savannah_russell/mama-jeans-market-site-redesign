<?php
	/*
	 * Template Name: Resources Page
	 * Creates the resources page for MaMa Jean's
	 */
	get_header();
	the_post();
?>
<section class="inner-section">
	<section class="v-divide cf chaparral resource">
		<aside class="v-left">
			<h1>MaMa Jean's Natural Market</h1>
			<?php the_content();?>
		</aside>
		<aside class="v-right">
			<h1 class="sky-blue">Email Us!</h1>
			<?php if(get_field('mj_store_email_addresses')): ?>

			<?php while(has_sub_field('mj_store_email_addresses')): ?>

			<h2><?php the_sub_field('mj_category_name'); ?>:</h2>
			<p><?php the_sub_field('mj_category_email_address'); ?></p>
			<?php endwhile; ?>
		<?php endif; ?>
		</aside>
	</section>
	<div class="grey divider"></div>
	<h1>Need one of our logos? Download them here!</h1>
	<?php if(get_field('mj_resource_images')): ?>

			<?php while(has_sub_field('mj_resource_images')): ?>
			<article class="download-img cf">
				<img src="<?php the_sub_field('mj_display_image');?>"/>
				<h2><?php the_sub_field('mj_image_description'); ?></h2><br />
				<h2><a href="<?php the_sub_field('mj_upload_image'); ?>">Download</a></h2>
			</article>
			<?php endwhile; ?>
		<?php endif; ?>
	<div class="grey divider"></div>
	<h3>Press Releases</h3>
	<?php if (get_field('mj_press_releases')): ?>
		<?php while(has_sub_field('mj_press_releases')): ?>
			<h2><?php the_sub_field('mj_press_release_name'); ?>
				<a class="sky-blue" href="<?php the_sub_field('mj_press_release_article')?>">Download</a>
			</h2>
		<?php endwhile; ?>
	<?php endif; ?>
</section>
<?php get_footer();?>