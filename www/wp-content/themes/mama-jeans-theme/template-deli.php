<?php
	/*
	 * Template Name: Deli Page
	 * Creates the deli page for MaMa Jean'sdepartment section
	 */
	get_header();
	the_post();
?>
<section class="inner-section">
	<div class="deli cf">
		<img src="<?php echo get_template_directory_uri(); ?>/images/mj-deli-logo.png"/>
		<div class="d-location">
			<h4>Two Locations:</h4>
			<p><span class="bebas-neue">1110 E. Republic Rd.</span><br/>
				<span class="call"><a href="tel:4178815326">(417) 881-5326</a></span>
			</p>
			<p class="dot bebas-neue">&#8226;</p>
			<div class="divider"></div>
			<p><span class="bebas-neue">3530 E. Sunshine</span><br />
				<span class="call"><a href="tel:4174291800">(417) 429-1800</a></span>
			</p>
			<div class="cf"></div>
			<a class="orange cf" href="#">See Menu & Hours</a>
		</div>
	</div>
	<div class="deli express cf">
		<img src="<?php echo get_template_directory_uri(); ?>/images/mj-deli-express-logo.png"/>
		<div class="d-location">
			<p><span class="bebas-neue">1727 S. Campbell Ave.</span><br/>
				<span class="call"><a href="tel:4178315229">(417) 831-5229</a></span>
			</p>
				<div class="cf"></div>
				<a class="orange" href="#">See Menu & Hours</a>

		</div>
	</div>
	<div class="cf"></div>
	<div class="our-deli">
		<?php
 
			$post_object = get_field('mj_read_more_post');
 
			if( $post_object ): 
 
			// override $post
			$post = $post_object;
			setup_postdata( $post ); 
		 
			?>
		<h4 class="no-margin"><?php the_title();?>:</h4>
		<?php the_excerpt();?>
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>
	</div>
	<section class="v-divide cf">
		<aside class="food-feed v-left">
			<h4>Food Feed</h4>
			<p>A look into the kitchen</p>
			<?php if(get_field('mj_deli_foodfeed')): ?>
				<?php while(has_sub_field('mj_deli_foodfeed')): ?>
					<img src="<?php the_sub_field('mj_deli_foodfeed_image'); ?>"/>
			<?php endwhile; endif; ?>
		</aside>
		<aside class="specials v-right">
			<h4>Monthly Specials</h4>
			<p>Click for details</p>
			<div class="divider"></div>
			<?php if(get_field('mj_monthly_specials')): ?>
				<?php while(has_sub_field('mj_monthly_specials')): ?>
					<img src="<?php the_sub_field('mj_image_for_special'); ?>" />
				<?php endwhile; endif; ?>

		</aside>
	</section>

</section>
<?php get_footer(); ?>