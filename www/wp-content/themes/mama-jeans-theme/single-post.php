<?php
	get_header();
	the_post();
?>
<section class="inner-section blog">
	<section class="v-divide cf">
		<div class="v-left">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</div>
		<div class="v-right">
			<h3>Popular</h3>
			<ul>
				<?php
						$popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
						while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
				<li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail();?></a>
					<p><strong><a href="<?php the_permalink(); ?>"><?php the_title();?></a></strong><br />
						<em><?php the_field('mj_post_subtitle'); ?></em></p>
				</li>
				<?php endwhile;?>
			</ul>
		</div>
	</div>
	</section>
	<aside class="cf">
		<h4 class="orange">More on this topic...</h4>
		<nav>
			<ul>
				<?php if(get_field('mj_locations')): ?>
				<?php while(has_sub_field('mj_locations')): ?>
				<?php $post_objects = get_sub_field('mj_blog_linked_post');?>


							<li>
								<a href="<?php get_permalink($post_objects->ID); ?>"><img src="<?php get_the_post_thumbnail($post_objects->ID); ?>"/></a>
							</li>

						

				<?php endwhile; endif; ?>
				</ul>
		</nav>
	</aside>
</section>
<?php get_footer() ?>