<?php
	/*
	 * Template Name: Location Page
	 * Creates the location page
	 */
	get_header();
	the_post();
?>
<section class="inner-section">
	<div class="google-maps-placeholder">
		<iframe width="1030" height="566" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?ie=UTF8&amp;q=mama+jeans&amp;fb=1&amp;gl=us&amp;hq=mama+jeans&amp;hnear=Springfield,+Greene,+Missouri&amp;t=m&amp;ll=37.173449,-93.25882&amp;spn=0.154837,0.346413&amp;z=12&amp;output=embed"></iframe><br />
	</div>
	<?php if(get_field('mj_locations')): ?>
	<?php while(has_sub_field('mj_locations')): ?>
	<article class="location">
		<div class="location-img">
			<img src="<?php the_sub_field('mj_store_picture'); ?>"/>
			<h3 class="red">2.3<br /><span>Miles</span></h3>
		</div>
		<header>
				<h4><?php the_sub_field('mj_location_name'); ?></h4>
				<p><?php the_sub_field('mj_address_line_1'); ?><br />
					<?php the_sub_field('mj_store_phone_number'); ?><br />
				<span><?php the_sub_field('mj_address_line_2'); ?></span>
				</p>
			<nav class="location-nav cf">
				<a class="orange" href="<?php the_sub_field('mj_store_directions_link');?>">Directions</a>
				<a class="sky-blue" href="<?php the_sub_field('mj_store_inside_view');?>">Look Inside</a>
			</nav>
		</header>
		<div class="cf"></div>
		<p>
			<?php the_sub_field('mj_store_description'); ?>
		</p>
	</article>
<?php endwhile; endif; ?>
</section>
<?php get_footer();?>