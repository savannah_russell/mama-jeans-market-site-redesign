<?php 
// you find the station ID on this page: http://weather.noaa.gov/
// 1. Select a state (under "United States Weather")
// 2. Select city
// 3. See URL for the all-caps, 4-letter station ID

function get_da_weather($station_id = 'KSGF'){
	//if it's been an hour since the last weather update
	if(time()-get_option('last_weather_check', 0)>3600){
	
		$xml = simplexml_load_file('http://www.weather.gov/xml/current_obs/' . $station_id . '.xml');
		$cur = array(
			'temp' => number_format(intval($xml->temp_f)),
			'condition' => strtolower((string)$xml->weather)
		);
		update_option('last_weather_check', time());
		update_option('cur_weather', $cur);
	}else{
		$cur = get_option('cur_weather');
	}
	return $cur;
}

function get_tweetage($username, $num_tweets = 1){
	$tweet = get_option('cur_tweet_' . $username);

	//check if its been 10 min
	if(time()-$tweet['lastupdated']>600){
	
		$xml = simplexml_load_file('http://api.twitter.com/1/statuses/user_timeline.xml?screen_name=' . $username . '&count=' . $num_tweets . '&trim_user=false&exclude_replies=true&include_rts=false');
		
		if(empty($xml->status))
			return $tweet;

		//get user info
		$user = $xml->status[0]->user;
		$tweet['user'] = array(
			'name'		=> $user->name,
			'img'		=> $user->profile_image_url,
			'img_https'	=> $user->profile_image_url_https,
			'desc'		=> $user->description,
			'location'	=> $user->location
		);

		//get tweets
		foreach($xml->status as $status){
			$tweet['tweets'][] = array(
				'tweet' => filter_tweet($status->text),
				'when' => time_since(strtotime($status->created_at))
			);
		}


		$tweet['lastupdated'] = time();
		
		update_option('cur_tweet_' . $username, $tweet);
			
	}
	return $tweet;
}
function filter_tweet($text){
	
	//turn URLs into links
	$pattern = "@\b(https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+:)?[0-9a-zA-Z_!~*'().&=+$%-]+\@)?(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/[0-9a-zA-Z_!~*'().;?:\@&=+$,%#-]+)*/?)@";
	$text = preg_replace($pattern, '<a href="\0">\0</a>', $text);
	
	//turn usernames into links
	$text = preg_replace('/@(\w+)/', '<a href="http://twitter.com/$1">@$1</a> ', $text);
	return $text;
}


function googleGeocode($address){
		
	$gmaps_apikey = ''; // DONT FORGET TO SET THIS!!!!!!!!!!!!!!!!!!!!!!!!
	
	$base_url = "http://maps.google.com/maps/geo?output=xml" . "&key=" . $gmaps_apikey;

    $request_url = $base_url . "&q=" . urlencode($address);
    $xml = simplexml_load_file($request_url);

    $status = $xml->Response->Status->code;
    if (strcmp($status, "200") == 0) {
    
		// Successful geocode
		$coordinates = $xml->Response->Placemark->Point->coordinates;
		$coordinatesSplit = split(",", $coordinates);
		
		// Format: Longitude, Latitude, Altitude
		$latlng = array($coordinatesSplit[1], $coordinatesSplit[0]);
		
		return $latlng;
      
    } else {
		// failure to geocode
		return false;
    }

}
