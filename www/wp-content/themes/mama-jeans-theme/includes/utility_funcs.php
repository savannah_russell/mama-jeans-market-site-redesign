<?php 
/* Works out the time since the entry post, takes a an argument in unix time (seconds) */
function time_since($original) {
    // array of time period chunks
    $chunks = array(
        array(60 * 60 * 24 * 365 , 'year'),
        array(60 * 60 * 24 * 30 , 'month'),
        array(60 * 60 * 24 * 7, 'week'),
        array(60 * 60 * 24 , 'day'),
        array(60 * 60 , 'hour'),
        array(60 , 'minute'),
    );
    
    $today = time(); /* Current unix time  */
    $since = $today - $original;
    
    // $j saves performing the count function each time around the loop
    for ($i = 0, $j = count($chunks); $i < $j; $i++) {
        
        $seconds = $chunks[$i][0];
        $name = $chunks[$i][1];
        
        // finding the biggest chunk (if the chunk fits, break)
        if (($count = floor($since / $seconds)) != 0) {
            // DEBUG print "<!-- It's $name -->\n";
            break;
        }
    }
    
    $print = ($count == 1) ? '1 '.$name : "$count {$name}s";
    
    /*
    if ($i + 1 < $j) {
        // now getting the second item
        $seconds2 = $chunks[$i + 1][0];
        $name2 = $chunks[$i + 1][1];
        
        // add second item if it's greater than 0
        if (($count2 = floor(($since - ($seconds * $count)) / $seconds2)) != 0) {
            $print .= ($count2 == 1) ? ', 1 '.$name2 : ", $count2 {$name2}s";
        }
    }
    */
    return $print . ' ago';
}


function forty_bulletize($str, $class=''){
	// Order of replacement
	$order   = array("\r\n", "\n", "\r");
	$replace = '</li><li>';

	// Processes \r\n's first so they aren't converted twice.
	$content = "<li>" . str_replace($order, $replace, $str) . "</li>";
	return str_replace('<li></li>', '', $content);
}

// Automatically turns carriage returns and newlines into <br />'s
function forty_breakify($str){
	// Order of replacement
	$order   = array("\r\n", "\n", "\r");
	$replace = '<br />';

	// Processes \r\n's first so they aren't converted twice.
	$content = str_replace($order, $replace, $str);
	return $content;
}

function forty_getallmeta($post_id, $filter){
	global $wpdb;
	
	$qry = "SELECT pm.meta_key mkey, pm.meta_value val FROM {$wpdb->posts} p, {$wpdb->postmeta} pm
		WHERE p.ID = pm.post_id
		AND p.post_status = 'publish'
		AND p.ID = $post_id";
	
	if(!empty($filter))
		$qry .= " AND pm.meta_key LIKE '$filter'";

	$results = $wpdb->get_results( $qry );
	
	$ret = array();
	foreach($results as $r){
		if(array_key_exists($r->mkey, $ret)){
			if(is_array($ret[$r->mkey]))
				$ret[$r->mkey][] = $r->val;
			else
				$ret[$r->mkey] = array($ret[$r->mkey], $r->val);
		}else
			$ret[$r->mkey] = maybe_unserialize( $r->val );
	}
		
	return $ret;
}

// More controllable excerpt function
function forty_excerpt($string, $limit=140, $break=".", $pad="...")
{

	$string = strip_tags($string, '<p><br><blockquote><code><ul><li><i><em><strong>');

  // return with no change if string is shorter than $limit
  if(strlen($string) <= $limit) return $string;

  // is $break present between $limit and the end of the string?
  if(false !== ($breakpoint = strpos($string, $break, $limit))) {
    if($breakpoint < strlen($string) - 1) {
      $string = substr($string, 0, $breakpoint) . $pad;
    }
  }
  
	$input=$string;
	$opened = array();
   
   // loop through opened and closed tags in order 
	if(preg_match_all("/<(\/?[a-z]+)>?/i", $input, $matches)) {
		foreach($matches[1] as $tag) {
			if(preg_match("/^[a-z]+$/i", $tag, $regs)) { // a tag has been opened
				if(strtolower($regs[0]) != 'br') $opened[] = $regs[0];
			} elseif(preg_match("/^\/([a-z]+)$/i", $tag, $regs)) { // a tag has been closed
				unset($opened[array_pop(array_keys($opened, $regs[1]))]);
			} 
		}
	}
	
	// close tags that are still open
	if($opened) { $tagstoclose = array_reverse($opened); foreach($tagstoclose as $tag) $input .= "</$tag>"; } 
    
  return $input;
}


function get_the_excerpt_here($post_id)
{
  global $wpdb;
  $query = "SELECT post_excerpt e, post_content c FROM {$wpdv->posts} WHERE ID = $post_id LIMIT 1";
  $result = $wpdb->get_results($query, ARRAY_A);
  if(!empty($result[0]['e']))
	return $result[0]['e'];
  else
  	return forty_excerpt($result[0]['c']);
}

// Returns timthumbified URL
function forty_timthumbify($src){

	global $blog_id;

	if(is_multisite()){
		$blog_upload_dir = get_site_url(1).'/wp-content/blogs.dir/'.$blog_id;
		$src = get_site_url(1).'/scripts/timthumb.php?src='.$blog_upload_dir.strstr($src, '/files/');
	}else
		$src = get_bloginfo('template_directory').'/includes/timthumb.php?src='.$src;
		
	return $src;	
}