<?php 
// http://codex.wordpress.org/Function_Reference/register_post_type

function register_CPTs() 
{

	/*

	// Duplicate this for each CPT.
	
	$labels = array(
		'name' => _x('Portfolio', 'post type general name'),
		'singular_name' => _x('Portfolio Item', 'post type singular name'),
		'add_new' => _x('Add New', 'portfolio'),
		'add_new_item' => __('Add New Portfolio Item'),
		'edit_item' => __('Edit Portfolio Item'),
		'new_item' => __('New Portfolio Item'),
		'view_item' => __('View Portfolio Item'),
		'search_items' => __('Search Portfolio'),
		'not_found' =>  __('No Portfolio Items found'),
		'not_found_in_trash' => __('No Portfolio Items found in Trash'), 
		'parent_item_colon' => '',
		'menu_name' => 'Portfolio'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'project'),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 21,
		'supports' => array('title','page-attributes')
	);
	
	register_post_type('portfolio',$args);
	

	*/


	// Custom Post Type: Departments
	
	$labels = array(
		'name' => _x('Departments', 'post type general name'),
		'singular_name' => _x('Department', 'post type singular name'),
		'add_new' => _x('Add New', 'Department'),
		'add_new_item' => __('Add New Department'),
		'edit_item' => __('Edit Department'),
		'new_item' => __('New Department'),
		'view_item' => __('View Department'),
		'search_items' => __('Search Departments'),
		'not_found' =>  __('No matching Departments found'),
		'not_found_in_trash' => __('No Departments found in Trash'), 
		'parent_item_colon' => '',
		'menu_name' => 'Departments'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'department'),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 21,
		'supports' => array('title','page-attributes', 'editor', 'thumbnail')
	);
	
	register_post_type('department',$args);
	
	// Custom Post type Event
	
	$labels = array(
		'name' => _x('Event', 'post type general name'),
		'singular_name' => _x('Event', 'post type singular name'),
		'add_new' => _x('Add New', 'Event'),
		'add_new_item' => __('Add New Event'),
		'edit_item' => __('Edit Event'),
		'new_item' => __('New Event'),
		'view_item' => __('View Event'),
		'search_items' => __('Search Events'),
		'not_found' =>  __('No Events found'),
		'not_found_in_trash' => __('No Events found in Trash'), 
		'parent_item_colon' => '',
		'menu_name' => 'Events'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'events'),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 21,
		'supports' => array('title','page-attributes', 'editor')
	);
	
	register_post_type('events',$args);

	//Custom Post Type Sales

		$labels = array(
		'name' => _x('Sale', 'post type general name'),
		'singular_name' => _x('Sale Item', 'post type singular name'),
		'add_new' => _x('Add New', 'Sale Item'),
		'add_new_item' => __('Add New Sale Item'),
		'edit_item' => __('Edit Sale Item'),
		'new_item' => __('New Sale Item'),
		'view_item' => __('View Sale Item'),
		'search_items' => __('Search Sale Items'),
		'not_found' =>  __('No Sale Items found'),
		'not_found_in_trash' => __('No Sale Items found in Trash'), 
		'parent_item_colon' => '',
		'menu_name' => 'Sales'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'project'),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => 21,
		'supports' => array('page-attributes', 'title')
	);
	
	register_post_type('sale',$args);
	
}

add_action('init', 'register_CPTs');