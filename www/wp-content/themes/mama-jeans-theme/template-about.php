<?php
	/*
	 * Template Name: About Page
	 * Creates the about page for MaMa Jean's, including a top slider and a popout department section
	 */
	get_header();
	the_post();
?>
<section class="inner-section">
	<div class="slider about">
	</div>
		<?php
 
			$post_object = get_field('mj_read_more_post');
 
			if( $post_object ): 
 
			// override $post
			$post = $post_object;
			setup_postdata( $post ); 
		 
			?>
	<h1 class="orange no-margin"><?php the_title();?>:</h1>
	<?php the_excerpt();?>
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>
	<div class="divider grey"></div>
	<h3 class="department-header">Departments</h3>
	<div class="department-list">
		<ul class="cf">
			<!--Fields for slider: mj_about_department_name, mj_about_department_description, mj_about_department_post_link (the_permalink, the_excerpt), mj_about_department_picture_large-->
			<?php if(get_field('mj_about_department_information')): ?>
				<?php while(has_sub_field('mj_about_department_information')): ?>
			<li>
				<img src="<?php the_sub_field('mj_about_department_picture_small');?>"/>
				<h4><?php the_sub_field('mj_about_department_name');?></h4>
			</li>
			<?php endwhile; endif; ?>
		</ul>
	</div>

</section>
<?php get_footer(); ?>