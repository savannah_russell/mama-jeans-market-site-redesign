<?php
	/*
	 * Template Name: Events Page
	 * Creates the event page for MaMa Jean's
	 */
	get_header();
	the_post();
?>
<section class="inner-section event-page cf">
	<header class="cf"><h3>Upcoming Events</h3></header>
	<section class="events">
		<!-- get all event posts with mj_event_date, mj_event_location, mj_event_time, the_title, and the_excerpt-->
		<article>
			<h3><!--mj_event_date as M d-->DATE</h3>
			<h4><!--the_title-->Title</h4>
			<!--the_excerpt--><p>Excerpt</p>
			<h4><!--mj_event_location-->Location <span><!--mj_event_time-->Time</span></h4>
		</article>
		<article>
			<h3><!--mj_event_date as M d-->DATE</h3>
			<h4><!--the_title-->Title</h4>
			<!--the_excerpt--><p>Excerpt</p>
			<h4><!--mj_event_location-->Location <span><!--mj_event_time-->Time</span></h4>
		</article>
		<article>
			<h3><!--mj_event_date as M d-->DATE</h3>
			<h4><!--the_title-->Title</h4>
			<!--the_excerpt--><p>Excerpt</p>
			<h4><!--mj_event_location-->Location <span><!--mj_event_time-->Time</span></h4>
		</article>
	</section>
	<aside class="living-room">
		<?php the_content();?>
		<!-- if( get_field('mj_events_lr_featured') ): while( has_sub_field('mj_events_lr_featured') ): ?>-->
		<!-- get post image and permalink-->
		<img src="<?php echo get_template_directory_uri(); ?>/images/events-placeholder.png"/>
	</aside>
</section>
<?php get_footer(); ?>