<?php
	/*
	 * Template Name: Search Page
	 * Creates the searc page for MaMa Jean's
	 */
	get_header();
	the_post();
?>
<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
	$query_split = explode("=", $string);
	$search_query[$query_split[0]] = urldecode($query_split[1]);
} // foreach

$search = new WP_Query($search_query);
?>
<?php
global $wp_query;
$total_results = $wp_query->found_posts;
?>
<section class="inner-section">
	<?php get_search_form(); ?>
	<section class="v-divide cf">		
		<div class="v-left results">
			<!--section for search results, with picture, title, and an excerpt. Only appears after a search-->
			<div class="results month">
				<!--I think it displays posts per month. Call in posts, sort by month somehow, loop it-->
			</div>
		</div>
		<div class="v-right pop-search">
			<h4>Popular Stories...</h4>
			<ul>
				<?php
					$popularpost = new WP_Query( array( 'posts_per_page' => 7, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
					while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>

					<li><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>

					<?php endwhile;	?>
			</ul>
		</div>
	</section>
</section>
<?php get_footer(); ?>