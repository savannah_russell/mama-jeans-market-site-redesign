<?php
	/*
	 * Template Name: Sales Page
	 * Creates the sales page for MaMa Jean's
	 */
	get_header();
	the_post();
?>
<section class="inner-section">
	<section class="sales">
		<?php the_post_thumbnail();?>
		<?php the_content();?>
	<!--new query-->
	 <?php $sales_query = new WP_Query(array( 'tag_name' =>'beer')); ?>
	 <div class="cf">
			<a href="#"><header class="sales-dropdown-grocery">Grocery</header></a>
			<div class="sales-hide-grocery">
	 <?php if ($sales_query->have_posts() ):
	 	while ( $sales_query->have_posts()): ?>
	 			<p>You did it!</p>
					<article>
						<img src="<?php the_field('mj_sales_image');?>">
						<ul>
							<li><?php the_field('mj_sales_brand');?></li>
							<li><?php the_title();?></li>
							<li><?php the_field('mj_sales_price');?></li>
							<li>you save <?php the_field('mj_sales_savings');?></li>
						</ul>
					</article>

<?php endwhile; ?>
<?php  endif; ?>
			</div>
		</div>
	</section>
	<aside>
	</aside>


</section>
<?php get_footer(); ?>