<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head profile="http://gmpg.org/xfn/11"> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php wp_title('&raquo;','true','right'); ?> <?php bloginfo('name') ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" type="text/css" media="screen" />
	<script type="text/javascript" src="//use.typekit.net/kgu6sqv.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<!--[if lt IE 9]>
	<script src="<?php bloginfo('template_directory') ?>/js/vendor/html5shiv.js"></script>
	<![endif]-->
    <?php
    	// <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    ?>
	<?php wp_head();?>	
</head> 
<body <?php body_class("store-open"); ?> >
	<header id="return">
		<div class="oc-message">
			<!-- we are opened or closed goes here --><h2>We are open BLAH to BLAH today!</h2>
		</div>
		<div class="branding-nav">
			<img src="<?php echo get_template_directory_uri(); ?>/images/header-logo.png"/>
			<a href="<?php bloginfo('url'); ?>/search"><img src="<?php echo get_template_directory_uri(); ?>/images/search.png"/ class="search"></a>
				<?php wp_nav_menu(array('menu' => 'mamas-menu', 'menu_class' => 'cf', 'container' => 'nav', 'container_class' => 'headnav cf', 'after' => '<li class="dot">&#8226;</li>'));?>
			
			<div class="cf"></div>
		</div>
		<div class="oc-border"></div>
	</header>