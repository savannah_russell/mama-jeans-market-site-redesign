<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1" />
	<title>40D SASS Structure</title>
	<!--[if lte IE 8]>
    	<link rel="stylesheet" href="ie.css">
	<![endif]-->
	<!--[if gt IE 8]><!-->
	    <link rel="stylesheet" href="style.css">
	<!--<![endif]-->
	<!--[if lt IE 9]>
	<script src="/js/vendor/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript" src="//use.typekit.net/tuw1waz.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head> 
<body>

<header>
	<div class="row">
		<h1>40D SASS Structure</h1>
	</div>
</header>

<div class="site-wrap row">
	<nav>
		<ul>
			<li><a href="#about">About</a></li>
			<li>
				<a href="#documentation">Documentation</a>
				<ul>
					<li><a href="#structure">Structure</a></li>
					<li><a href="#mixins">Mixins</a></li>
					<li><a href="#modules">Modules</a></li>
					<li><a href="#partials">Partials</a></li>
					<li><a href="#vendor">Vendor</a></li>
				</ul>
			</li>			
		</ul>
	</nav>